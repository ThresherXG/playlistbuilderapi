﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using playlist_api.Services;
using playlist_api.Data.Repository.Playlist_Repository;
using playlist_api.Data.Repository.Tracks_Repository;
using playlist_api.Models.Pandora;
using playlist_api.Services.Pandora;
using System.Threading.Tasks;
using System;

namespace playlist_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PandoraController : ControllerBase
    {
        private readonly IPandoraScrapper _scrapper;
        private readonly IHelper _helper;
        private readonly IPlaylistRepository _playlistRepo;
        private readonly ITracksRepository _tracksRepo;

        public PandoraController(IPandoraScrapper scrapper, ITracksRepository tracksRepo, IPlaylistRepository playlistRepo, IHelper helper)
        {
            _scrapper = scrapper;
            _helper = helper;
            _tracksRepo = tracksRepo;
            _playlistRepo = playlistRepo;
        }

        [Authorize]
        [HttpPost]
        [Route("station")]
        public async Task<ActionResult> addPandoraStation([FromBody] PandoraPayloadModel model)
        {
            if(model.pandora_station_url == null || !model.pandora_station_url.Contains("https://www.pandora.com/station/"))
            {
                return BadRequest("Invalid Pandora Station Url");
            }

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if(playlist == null) 
            {
                return NotFound();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            if(playlist.account_id != currentUser) 
            {
                return Forbid();
            }

            var AuthDict = await _scrapper.refreshPandoraTokens();

            if( AuthDict["authToken"] == null || AuthDict["csrfToken"] == null || AuthDict.Count == 0)
            {
                return BadRequest("Pandora authentication failed");
            }

            var stationTracks = await _scrapper.scrapePandoraStation(model, AuthDict);

            if(stationTracks.Count == 0 || stationTracks == null) 
            {
                return NotFound("Pandora Station not found");
            }
   
            await _tracksRepo.bulkCreateTracks(stationTracks);
            
            await _tracksRepo.deleteDuplicatePlaylistTracks(model.playlist_id);

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("playlist")]
        public async Task<ActionResult> addPandoraPlaylist([FromBody] PandoraPayloadModel model)
        {
            if (model.pandora_playlist_url == null || !model.pandora_playlist_url.Contains("https://www.pandora.com/playlist/"))
            {
                return BadRequest("Invalid Pandora Playlist Url");
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if(playlist == null) 
            {
                return NotFound();
            }

            if(currentUser != playlist.account_id) 
            {
                return Forbid();
            }

            var AuthDict = await _scrapper.refreshPandoraTokens();

            if (AuthDict["authToken"] == null || AuthDict["csrfToken"] == null || AuthDict.Count == 0)
            {
                return BadRequest("Pandora authentication failed");
            }

            var playlistTracks = await _scrapper.scrapePandoraPlaylist(model, AuthDict);

            if(playlistTracks.Count == 0)
            {
                return NotFound("Pandora Playlist not found");
            }
           
            await _tracksRepo.bulkCreateTracks(playlistTracks);
           
            await _tracksRepo.deleteDuplicatePlaylistTracks(model.playlist_id);

            return Ok();
        }
    }
}

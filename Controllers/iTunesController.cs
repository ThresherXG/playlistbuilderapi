﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using playlist_api.Data.Repository.Playlist_Repository;
using playlist_api.Data.Repository.Tracks_Repository;
using playlist_api.Services;
using playlist_api.Services.iTunes;
using System;
using System.Threading.Tasks;

namespace playlist_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class iTunesController : ControllerBase
    {
        private readonly IHelper _helper;
        private readonly IiTunesProcessor _itunesProcessor;
        private readonly IPlaylistRepository _playlistRepo;
        private readonly ITracksRepository _tracksRepo;
        public iTunesController(IHelper helper, IiTunesProcessor itunesProcessor, IPlaylistRepository playlistRepo, ITracksRepository tracksRepo)
        {
            _helper = helper;
            _itunesProcessor = itunesProcessor;
            _playlistRepo = playlistRepo;
            _tracksRepo = tracksRepo;
        }

        [Authorize]
        [HttpPost]
        [Route("upload/{playlist_id}")]
        public async Task<ActionResult> itunesFileUpload(string playlist_id)
        {
            var files = HttpContext.Request.Form.Files;

            if (files.Count == 0 || playlist_id == null)
            {
                return BadRequest();
            }

            if (!Guid.TryParse(playlist_id, out var tempGuid))
            {
                return BadRequest();
            }
            
            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(Guid.Parse(playlist_id));

            if(playlist == null) 
            {
                return NotFound();
            }

            if(playlist.account_id != currentUser)
            {
                return Forbid();
            }

            var alliTunesTracks = await _itunesProcessor.processiTunesTXTFile(files, playlist_id);

            if(alliTunesTracks.Count == 0)
            {
                return BadRequest();
            }

            await _tracksRepo.bulkCreateTracks(alliTunesTracks);

            return Ok();
        }

    }
}

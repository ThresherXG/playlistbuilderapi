﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using playlist_api.Services;
using playlist_api.Data;
using playlist_api.Models;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace playlist_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IHelper _helper;
        private readonly IAccountsRepository _repository;

        public AuthController(IConfiguration config, IAccountsRepository repository, IHelper helper)
        {
            _config = config;
            _helper = helper;
            _repository = repository;
        }
     
        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<ActionResult> Register([FromBody] AccountsModel model) 
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Please enter a valid User Name, Email Address and Password");
            }

            var userName = await _repository.GetAccountByUserName(model.user_name);

            if(userName != null) 
            {
                return BadRequest("User is already registered, please log in");
            }

            var account = await _repository.GetAccountByEmail(model.email_address);

            if (account == null && userName == null)
            {
                string passwordHash = BCrypt.Net.BCrypt.HashPassword(model.password);

                model.password = passwordHash;

                await _repository.Create(model);

                var token = _helper.GenerateJSONWebToken(model, _config);

                return Ok(token);
            }

            return BadRequest("User is already registered, please log in");
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> Login([FromBody] LoginModel model)
        {
            if(!ModelState.IsValid)
            {
               return BadRequest("Invalid Email or Password");
            }

            var account = await _repository.GetAccountByEmail(model.email_address);

            if(account == null)
            {
                return BadRequest("Invalid Email or Password");
            }

            bool verifyPassword = BCrypt.Net.BCrypt.Verify(model.password, account.password);

            if (!verifyPassword) 
            {
                return BadRequest("Invalid Email or Password");
            }

            var token = _helper.GenerateJSONWebToken(account, _config);
                
            return Ok(token);
        }

        [Authorize]
        [HttpGet]
        [Route("verify")]
        public ActionResult verifyToken()
        {
            return Ok();
        }
    }
}

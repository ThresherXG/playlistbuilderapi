﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using playlist_api.Data;
using playlist_api.Services;
using playlist_api.Models.Spotify;
using playlist_api.Services.Spotify;
using Microsoft.AspNetCore.Authorization;
using playlist_api.Data.Repository.Tracks_Repository;
using playlist_api.Data.Repository.Playlist_Repository;
using System.Text.Json;
using Microsoft.Extensions.Configuration;

namespace playlist_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpotifyController : ControllerBase
    {
        private readonly IAccountsRepository _accountsRepo;
        private readonly ISpotifyAccountsRepository _spotifyProfileRepo;
        private readonly ITracksRepository _tracksRepo;
        private readonly ISpotifyTracksRepository _spotifyTracksRepo;
        private readonly IHelper _helper;
        private readonly ISpotifyProcessor _processor;
        private readonly IPlaylistRepository _playlistRepo;
        private readonly IConfiguration _config;

        public SpotifyController(IAccountsRepository accountsRepo, ISpotifyAccountsRepository spotifyProfileRepo, IHelper helper, ISpotifyProcessor processor, ITracksRepository tracksRepo, IPlaylistRepository playlistRepo, ISpotifyTracksRepository spotifyTracksRepo, IConfiguration config)
        {
            _accountsRepo = accountsRepo;
            _spotifyProfileRepo = spotifyProfileRepo;
            _tracksRepo = tracksRepo;
            _helper = helper;
            _processor = processor;
            _playlistRepo = playlistRepo;
            _spotifyTracksRepo = spotifyTracksRepo;
            _config = config;
        }

        [Authorize]
        [HttpPost]
        [Route("playlist")]
        public async Task<ActionResult> addSpotifyPlaylist(SpotifyPayloadModel model)
        {
            if (model.spotify_playlist_url == null || !model.spotify_playlist_url.Contains("https://open.spotify.com/playlist/") || !ModelState.IsValid)
            {
                return BadRequest("Invaild Spotify Playlist Url");
            }

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if (playlist == null)
            {
                return NotFound();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            if (playlist.account_id != currentUser)
            {
                return Forbid();
            }

            var spotifyProfile = await _spotifyProfileRepo.GetSpotfiyAccountById(currentUser);

            if (spotifyProfile == null)
            {
                return BadRequest("Please link your Spotify account");
            }

            if (_processor.expiredSpotifyTokenCheck(spotifyProfile))
            {
                await _processor.refreshSpotifyToken(_spotifyProfileRepo, spotifyProfile, _config);
            }

            var spotifyPlaylistLists = await _processor.scrapeSpotifyPlaylist(model, spotifyProfile);

            if (spotifyPlaylistLists.TracksList.Count == 0 || spotifyPlaylistLists.SpotifyTracksList.Count == 0  || spotifyPlaylistLists == null)
            {
                return NotFound("Spotify Playlist not found");
            }
     
            await _tracksRepo.bulkCreateTracks(spotifyPlaylistLists.TracksList);

            await _spotifyTracksRepo.bulkCreateSpotifyTracks(spotifyPlaylistLists.SpotifyTracksList);
            
            await _tracksRepo.deleteDuplicatePlaylistTracks(model.playlist_id);

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("album")]
        public async Task<ActionResult> addSpotifyAlbum(SpotifyPayloadModel model)
        {
            if (model.spotify_album_url == null || !model.spotify_album_url.Contains("https://open.spotify.com/album/") || !ModelState.IsValid)
            {
                return BadRequest("Invaild Spotify Album Url");
            }

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if (playlist == null)
            {
                return NotFound();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            if (playlist.account_id != currentUser)
            {
                return Forbid();
            }

            var spotifyProfile = await _spotifyProfileRepo.GetSpotfiyAccountById(currentUser);

            if (spotifyProfile == null)
            {
                return BadRequest("Please link your Spotify account");
            }

            if (_processor.expiredSpotifyTokenCheck(spotifyProfile))
            {
                await _processor.refreshSpotifyToken(_spotifyProfileRepo, spotifyProfile, _config);
            }

            var spotifyPlaylistLists = await _processor.scrapeSpotifyAlbum(model, spotifyProfile);

            if (spotifyPlaylistLists.TracksList.Count == 0 || spotifyPlaylistLists.SpotifyTracksList.Count == 0 || spotifyPlaylistLists == null)
            {
                return NotFound("Spotify Album not found");
            }

            await _tracksRepo.bulkCreateTracks(spotifyPlaylistLists.TracksList);

            await _spotifyTracksRepo.bulkCreateSpotifyTracks(spotifyPlaylistLists.SpotifyTracksList);

            await _tracksRepo.deleteDuplicatePlaylistTracks(model.playlist_id);

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("track")]
        public async Task<ActionResult> addSpotifyTrack(SpotifyPayloadModel model)
        {
            if (model.spotify_track_url == null || !model.spotify_track_url.Contains("https://open.spotify.com/track/") || !ModelState.IsValid)
            {
                return BadRequest("Invaild Spotify Track Url");
            }

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if (playlist == null)
            {
                return NotFound();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            if (playlist.account_id != currentUser)
            {
                return Forbid();
            }

            var spotifyProfile = await _spotifyProfileRepo.GetSpotfiyAccountById(currentUser);

            if (spotifyProfile == null)
            {
                return BadRequest("Please link your Spotify account");
            }

            if (_processor.expiredSpotifyTokenCheck(spotifyProfile))
            {
                await _processor.refreshSpotifyToken(_spotifyProfileRepo, spotifyProfile, _config);
            }

            var spotifyTracks = await _processor.scrapeSpotifyTrack(model, spotifyProfile);

            if (spotifyTracks.Track == null || spotifyTracks.SpotifyTrack == null || spotifyTracks == null)
            {
                return NotFound("Spotify Track not found");
            }

            await _tracksRepo.Create(spotifyTracks.Track);

            await _spotifyTracksRepo.Create(spotifyTracks.SpotifyTrack);

            await _tracksRepo.deleteDuplicatePlaylistTracks(model.playlist_id);

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("playlist/create")]
        public async Task<ActionResult> generateSpotifyPlaylist(SpotifyPayloadModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if (playlist == null)
            {
                return NotFound();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            if (playlist.account_id != currentUser)
            {
                return Forbid();
            }

            var tracksCountForPlaylists = await _tracksRepo.countTracksByPlaylistId(playlist.playlist_id);

            if(tracksCountForPlaylists < 1)
            {
                return BadRequest("Please add songs to the Playlist before uploading to Spotify");
            }

            var spotifyProfile = await _spotifyProfileRepo.GetSpotfiyAccountById(currentUser);

            if (spotifyProfile == null)
            {
                return BadRequest("Please link your Spotify account");
            }

            if (_processor.expiredSpotifyTokenCheck(spotifyProfile))
            {
                await _processor.refreshSpotifyToken(_spotifyProfileRepo, spotifyProfile, _config);
            }

            await _tracksRepo.bulkUpdateSpotifyIdsFromSpotifyTracks(playlist.playlist_id);

            var nullSpotifyIdTracks = await _tracksRepo.getAllTracksByPlaylistIdWhereSpotifyIdisNull(playlist.playlist_id);

            if (nullSpotifyIdTracks.Count != 0)
            {
                var searchResults = await _processor.searchSpotifyForTrackIds(nullSpotifyIdTracks, spotifyProfile);
           
                await _spotifyTracksRepo.bulkCreateSpotifyTracks(searchResults.SpotifyTracksList);
              
                await _tracksRepo.bulkUpdateTracks(searchResults.TracksUpdatedList);
            }         

            var createdSpotifyPlaylist = await _processor.createSpotifyPlaylist(playlist.playlist_name, spotifyProfile);

            var userTrackUris = await _tracksRepo.getAllTrackUrisByPlaylistId(playlist.playlist_id);

            var uriJsonLists = _processor.createUriTracksBody(userTrackUris);

            foreach(var uriJsonList in uriJsonLists) 
            {
                await _processor.addTracksSpotifyPlaylist(createdSpotifyPlaylist.Id, uriJsonList, spotifyProfile);
            }

            playlist.spotify_created = true;

            await _playlistRepo.Update(playlist);

            return Ok();
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using playlist_api.Services;
using playlist_api.Data.Repository.Playlist_Repository;
using playlist_api.Data.Repository.Tracks_Repository;
using playlist_api.Models;
using System.Threading.Tasks;
using playlist_api.Data;
using System.Linq;

namespace playlist_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TracksController : ControllerBase
    {
        private readonly ITracksRepository _tracksRepo;
        private readonly IPlaylistRepository _playlistRepo;
        private readonly IAccountsRepository _accountsRepo;
        private readonly IHelper _helper;

        public TracksController(ITracksRepository tracksRepo, IHelper helper, IPlaylistRepository playlistRepo, IAccountsRepository accountsRepo)
        {
            _tracksRepo = tracksRepo;
            _playlistRepo = playlistRepo;
            _accountsRepo = accountsRepo;
            _helper = helper;
        }

        [Authorize]
        [HttpPost]
        [Route("add")]
        public async Task<ActionResult> addTrack([FromBody] TracksModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if(currentUser != playlist.account_id)
            {
                return Forbid();
            }

            if(playlist == null)
            {
                return NotFound();
            }

            var account = await _accountsRepo.GetAccountById(currentUser);

            model.source = "Added by: " + account.user_name;
            model.source_name = "Added by: " + account.user_name;
            model.source_url = "text/plain";

            await _tracksRepo.Create(model);

            return Ok();
        }

        [Authorize]
        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult> deleteTrack([FromBody] TracksModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if (currentUser != playlist.account_id)
            {
                return Forbid();
            }

            if (playlist == null)
            {
                return NotFound();
            }

            await _tracksRepo.Delete(model);

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("update")]
        public async Task<ActionResult> updateTrack([FromBody] TracksModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            var trackToUpdate = await _tracksRepo.getTrackByTrackId(model.track_id);

            if (currentUser != playlist.account_id)
            {
                return Forbid();
            }

            if (playlist == null)
            {
                return NotFound();
            }

            trackToUpdate.title = model.title;
            trackToUpdate.artist = model.artist;
            trackToUpdate.album = model.album;
            
            await _tracksRepo.Update(trackToUpdate);

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("id")]
        public async Task<ActionResult> getTrackById([FromBody] TracksModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if (currentUser != playlist.account_id)
            {
                return Forbid();
            }

            if (playlist == null)
            {
                return NotFound();
            }

            var trackById = await _tracksRepo.getTrackByTrackId(model.track_id);

            return Ok(trackById);
        }

        [Authorize]
        [HttpDelete]
        [Route("bulkdelete")]
        public async Task<ActionResult> bulkDeleteTracks([FromBody] TracksModel[] model)
        {
            if (!ModelState.IsValid || model == null)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(model[0].playlist_id);

            if (currentUser != playlist.account_id)
            {
                return Forbid();
            }

            if (playlist == null)
            {
                return NotFound();
            }

            await _tracksRepo.bulkDeleteTracks(model);

            return Ok(model);
        }
    }
}

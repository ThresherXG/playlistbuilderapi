﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using playlist_api.Data.Repository.Playlist_Repository;
using playlist_api.Data.Repository.Tracks_Repository;
using playlist_api.DTOs;
using playlist_api.Models;
using playlist_api.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace playlist_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaylistsController : ControllerBase
    {
        private readonly IPlaylistRepository _playlistRepo;
        private readonly ITracksRepository _tracksRepo;
        private readonly IHelper _helper;
        private readonly IMapper _mapper;

        public PlaylistsController(IHelper helper, IPlaylistRepository playlistRepo, IMapper mapper, ITracksRepository tracksRepo)
        {
            _playlistRepo = playlistRepo;
            _tracksRepo = tracksRepo;
            _helper = helper;
            _mapper = mapper;
        }

        [Authorize]
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult> createPlaylist([FromBody] PlaylistModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            model.account_id = currentUser;
            model.spotify_created = false;

            await _playlistRepo.Create(model);

            return Ok();
        }

        [Authorize]
        [HttpPatch]
        [Route("update")]
        public async Task<ActionResult> updatePlaylist([FromBody] PlaylistModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlistToUpdate = await _playlistRepo.getPlaylistById(model.playlist_id);

            if (playlistToUpdate == null)
            {
                return NotFound();
            }

            if (currentUser != playlistToUpdate.account_id)
            {
                return Forbid();
            }

            playlistToUpdate.playlist_name = model.playlist_name;

            await _playlistRepo.Update(playlistToUpdate);

            return Ok();
        }

        [Authorize]
        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult> deletePlaylist([FromBody] PlaylistModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlistToDelete = await _playlistRepo.getPlaylistById(model.playlist_id);

            if(playlistToDelete == null) 
            {
                return NotFound();
            }

            if(currentUser != playlistToDelete.account_id) 
            {
                return Forbid(); ;
            }

            await _tracksRepo.deleteAllTracksByPlaylistId(model.playlist_id);
            await _playlistRepo.Delete(playlistToDelete);

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("id")]
        public async Task<ActionResult> getPlaylistById([FromBody] PlaylistModel model) 
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if(playlist == null) 
            {
               return NotFound();
            }

            if (currentUser != playlist.account_id)
            {
                return Forbid();
            }

            return Ok(_mapper.Map<PlaylistDTO>(playlist));
        }

        [Authorize]
        [HttpGet]
        [Route("all")]
        public async Task<ActionResult> getAllPlaylists()
        {
            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var allPlaylists =  await _playlistRepo.getAllPlaylistsByUserId(currentUser);

            if(allPlaylists == null) 
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IEnumerable<PlaylistDTO>>(allPlaylists));
        }

        [Authorize]
        [HttpPost]
        [Route("tracks")]
        public async Task<ActionResult> getAllTracks([FromBody] PlaylistModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var playlist = await _playlistRepo.getPlaylistById(model.playlist_id);

            if (playlist == null)
            {
                return NotFound();
            }

            if (currentUser != playlist.account_id)
            {
                return Forbid();
            }

            var allTracks = await _tracksRepo.getAllTracksByPlaylistId(model.playlist_id);

            return Ok(_mapper.Map<IEnumerable<TracksDTO>>(allTracks));
        }
    }
}

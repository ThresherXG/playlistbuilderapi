﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using playlist_api.Services;
using playlist_api.Data;
using playlist_api.Profiles;
using SpotifyAPI.Web;
using System;

using System.Threading.Tasks;

namespace playlist_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpotifyAuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IAccountsRepository _accountsRepo;
        private readonly ISpotifyAccountsRepository _spotifyAccountsRepo;
        private readonly IHelper _helper;
        private readonly CustomMapperProfile _customerMapper;

        public SpotifyAuthController(IConfiguration config, IAccountsRepository accountsRepo, ISpotifyAccountsRepository spotifyAccountsRepo, CustomMapperProfile customerMapper, IHelper helper)
        {
            _config = config;
            _accountsRepo = accountsRepo;
            _spotifyAccountsRepo = spotifyAccountsRepo;
            _customerMapper = customerMapper;
            _helper = helper;
        }

        [Authorize]
        [HttpGet]
        [Route("link")]
        public async Task<ActionResult> spotifyAuth()
        {
            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            var spotifyAccount = await _spotifyAccountsRepo.GetSpotfiyAccountById(currentUser);

            if(spotifyAccount != null)
            {
                return BadRequest();
            }

            var loginRequest = new LoginRequest(
            new Uri(_config["Spotify:redirectUrl"]),
            _config["Spotify:clientId"],
            LoginRequest.ResponseType.Code)
            {
                //Scopes: user-read-private user-read-email playlist-modify-public playlist-modify-private playlist-read-private playlist-read-collaborative
                Scope = new[] { Scopes.UserReadPrivate, Scopes.UserReadEmail, Scopes.PlaylistModifyPublic, Scopes.PlaylistModifyPrivate, Scopes.PlaylistReadPrivate, Scopes.PlaylistReadCollaborative },
                State = currentUser.ToString()
            };

            var uri = loginRequest.ToUri();

            return Ok(uri.ToString());
        }

        [HttpGet]
        [Route("confirm")]
        public async Task<ActionResult> getToken(string code, string state)
        {
            if (code == null || state == null)
            {
                return new ContentResult
                {
                    ContentType = "text/html",
                    Content = "<script>window.close()</script>"
                };
            }

            var account_id = Guid.Parse(state);

            var currentUser = await _accountsRepo.GetAccountById(account_id);

            currentUser.spotify_link = true;

            var response = await new OAuthClient().RequestToken(
            new AuthorizationCodeTokenRequest(_config["Spotify:clientId"], _config["Spotify:clientSecret"], code, new Uri(_config["Spotify:redirectUrl"])));

            var spotify = new SpotifyClient(response.AccessToken);

            var spotifyUser = await spotify.UserProfile.Current();

            var _spotifyAccount = _customerMapper.mapSpotifyAccountProfile(account_id, spotifyUser, response);

            await _spotifyAccountsRepo.Create(_spotifyAccount);
 
            await _accountsRepo.Update(currentUser);

            return new ContentResult
            {
                ContentType = "text/html",
                Content = "<script>window.close()</script>"
            };
        }

        [Authorize]
        [HttpDelete]    
        [Route("unlink")]
        public async Task<ActionResult> unLinkSpotifyAccount()
        {
            var currentUser =_helper.getUserFromJWT(HttpContext.User);

            var account = await _accountsRepo.GetAccountById(currentUser);

            if(account.spotify_link == true)
            {
                var spotifyAccount = await _spotifyAccountsRepo.GetSpotfiyAccountById(currentUser);

                account.spotify_link = false;

                await _accountsRepo.Update(account);

                await _spotifyAccountsRepo.Delete(spotifyAccount);

                return Ok("Unlinked");
            }

            return Ok("No spotfiy account found");
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using playlist_api.Data.Repository.Message_Repository;
using playlist_api.Models;
using playlist_api.Services;
using System.Threading.Tasks;

namespace playlist_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageRepository _messageRepo;
        private readonly IHelper _helper;
        public MessageController(IMessageRepository messageRepo, IHelper helper)
        {
            _messageRepo = messageRepo;
            _helper = helper;
        }

        [Authorize]
        [HttpPost]
        [Route("send")]
        public async Task<ActionResult> sendMessage(MessageModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var currentUser = _helper.getUserFromJWT(HttpContext.User);

            model.account_id = currentUser;

            await _messageRepo.Create(model);
            
            return Ok();
        }
    }
}

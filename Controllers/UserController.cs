﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using playlist_api.Services;
using playlist_api.Data;
using playlist_api.DTOs;
using playlist_api.Models;
using System;
using System.Threading.Tasks;

namespace playlist_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IHelper _helper;
        private readonly IAccountsRepository _accountsRepo;
        private readonly ISpotifyAccountsRepository _spotifyProfileRepo;
        private readonly IMapper _mapper;

        public UserController(IHelper helper, IAccountsRepository accountsRepo, IMapper mapper, ISpotifyAccountsRepository spotifyProfileRepo) 
        {
            _helper = helper;
            _accountsRepo = accountsRepo;
            _spotifyProfileRepo = spotifyProfileRepo;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet]
        [Route("profile")]
        public async Task<ActionResult> getProfile()
        {
            Guid currentUser = _helper.getUserFromJWT(HttpContext.User);

            var account = await _accountsRepo.GetAccountById(currentUser);

            var userProfile = _mapper.Map<AccountsModel, UserProfileDTO>(account);

            if (account.spotify_link  == true) 
            {
                var spotifyProfile = await _spotifyProfileRepo.GetSpotfiyAccountById(currentUser);

                userProfile.spotify_display_name = spotifyProfile.spotify_display_name;

                userProfile.spotify_account_url = spotifyProfile.spotify_account_url;

                return Ok(userProfile);
            }

            return Ok(userProfile);
        }
    }
}

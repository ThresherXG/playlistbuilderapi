﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace playlist_api.Migrations
{
    public partial class spotifyuriadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "spotify_uri",
                table: "tracks",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "spotify_uri",
                table: "spotify_tracks",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "spotify_uri",
                table: "tracks");

            migrationBuilder.DropColumn(
                name: "spotify_uri",
                table: "spotify_tracks");
        }
    }
}

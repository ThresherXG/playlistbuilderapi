﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace playlist_api.Migrations
{
    public partial class updatedtracks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "spotify_exists",
                table: "tracks");

            migrationBuilder.DropColumn(
                name: "first_name",
                table: "accounts");

            migrationBuilder.DropColumn(
                name: "last_name",
                table: "accounts");

            migrationBuilder.AddColumn<string>(
                name: "spotify_track_id",
                table: "tracks",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "spotify_track_id",
                table: "tracks");

            migrationBuilder.AddColumn<bool>(
                name: "spotify_exists",
                table: "tracks",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "first_name",
                table: "accounts",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "last_name",
                table: "accounts",
                type: "character varying(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "");
        }
    }
}

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using playlist_api.Models;
using Microsoft.EntityFrameworkCore;
using playlist_api.Data;
using playlist_api.Services;
using playlist_api.Profiles;
using Microsoft.AspNetCore.Diagnostics;
using playlist_api.Data.Repository.Playlist_Repository;
using playlist_api.Data.Repository.Tracks_Repository;
using System.Net.Http.Headers;
using playlist_api.Services.Pandora;
using playlist_api.Services.iTunes;
using Microsoft.AspNetCore.Mvc;
using playlist_api.Services.Spotify;
using playlist_api.Data.Repository;
using playlist_api.Data.Repository.Message_Repository;

namespace playlist_api
{
    public class Startup
    {
        public IConfiguration _config;

        public Startup(IConfiguration config)
        {
            _config = config;
        }

        private readonly string _policyName = "CorsPolicy";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //turn off auto model validation 
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            //json trimming service
            services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new TrimStringConverter()));
                
            //controllers service
            services.AddControllers();

            //automapper service
            services.AddAutoMapper(typeof(Startup));

            //database service
            services.AddDbContext<PlaylistBuilderContext>(options => options.UseNpgsql(_config["ConnectionStrings:DefaultConnection"]));

            //dependency injection
            services.AddScoped(typeof(IAccountsRepository), typeof(AccountsRepository));
            services.AddScoped(typeof(ISpotifyAccountsRepository), typeof(SpotifyAccountsRepository));
            services.AddScoped(typeof(IPlaylistRepository), typeof(PlaylistRepository));
            services.AddScoped(typeof(ISpotifyTracksRepository), typeof(SpotifyTracksRepository));
            services.AddScoped(typeof(ITracksRepository), typeof(TracksRepository));
            services.AddScoped(typeof(IPandoraScrapper), typeof(PandoraScrapper));
            services.AddScoped(typeof(ISpotifyProcessor), typeof(SpotifyProcessor));
            services.AddScoped(typeof(IiTunesProcessor), typeof(iTunesProcessor));
            services.AddScoped(typeof(IHelper), typeof(Helper));
            services.AddScoped(typeof(IMessageRepository),typeof(MessageRepository));
            services.AddScoped(typeof(CustomMapperProfile));

            //logging service
            services.AddLogging(options =>
            {
                options.AddSimpleConsole(x =>
                {
                    x.TimestampFormat = "[MM-dd-yyyy HH:mm:ss]";
                });
            });

            //http spotify client service
            services.AddHttpClient("Spotify", options =>
            {
                options.BaseAddress = new Uri("https://api.spotify.com/v1/");
                options.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });

            //http pandora client service
            services.AddHttpClient("Pandora", options =>
            {
                options.BaseAddress = new Uri("https://www.pandora.com/");
                options.DefaultRequestHeaders.Host = "www.pandora.com";
                options.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                options.DefaultRequestHeaders.Add("user-agent",  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36");
            });

            //auth jwt service
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = _config["JWT:Issuer"],
                        ValidAudience = _config["JWT:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWT:Key"]))
                    };
             });

            //cors policy
            services.AddCors(opt =>
            {
                opt.AddPolicy(name: _policyName, builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
        }

        //This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            
            //error handleing middleware
            app.UseExceptionHandler(a => a.Run(async context =>
            {
                var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                var exception = exceptionHandlerPathFeature.Error;

                Console.WriteLine(exception.Message);

                //await context.Response.WriteAsJsonAsync(new { error = exception.Message });
            }));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(_policyName);

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

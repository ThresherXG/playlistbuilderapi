﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace playlist_api.Models.Spotify
{
    public class SpotifyTracksModel
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int spotify_tracks_index { get; set; }

        [Required]
        public string spotify_track_id { get; set; }

        [Required]
        public string spotify_uri { get; set; }

        [Required]
        public string title { get; set; }

        [Required]
        public string album { get; set; }

        [Required]
        public string artist { get; set; }

        [Required]
        public DateTime created { get; set; } = DateTime.UtcNow;
    }
}

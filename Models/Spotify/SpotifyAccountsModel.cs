﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace playlist_api.Models
{
    public class SpotifyAccountsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int spotify_account_index { get; set; }

        [Required]
        public string spotify_display_name { get; set; }

        [Required]
        public string spotify_account_id { get; set; }

        [EmailAddress]
        public string spotify_email { get; set; }

        [Required]
        public string spotify_account_url { get; set; }

        [Required]
        public string access_token { get; set; }

        [Required]
        public string refresh_token { get; set; }

        [Required]
        public DateTime token_created { get; set; }

        [Required]
        public int expires_in { get; set; }

        [Required]
        public string country { get; set; }

        [Required]
        public Guid account_id { get; set; }

        [Required]
        public DateTime created { get; set; } = DateTime.UtcNow;
    }
}

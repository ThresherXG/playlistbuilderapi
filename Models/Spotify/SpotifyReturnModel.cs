﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Models.Spotify
{
    public class SpotifyReturnModel
    {
        public List<TracksModel> TracksList { get; set; }
        public List<SpotifyTracksModel> SpotifyTracksList { get; set; }
        public List<TracksModel> TracksUpdatedList { get; set; }
        public TracksModel Track { get; set; }
        public SpotifyTracksModel SpotifyTrack { get; set; }

    }
}

﻿using System.Collections.Generic;

namespace playlist_api.Models.Spotify
{
    public class SpotifyUriModel
    {
        public List<string> uris { get; set; }
    }
}

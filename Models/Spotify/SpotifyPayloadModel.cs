﻿using System;
using System.ComponentModel.DataAnnotations;

namespace playlist_api.Models.Spotify
{
    public class SpotifyPayloadModel
    {
        [Required]
        public Guid playlist_id { get; set; }
        public string spotify_playlist_url { get; set; }
        public string spotify_track_url { get; set; }
        public string spotify_album_url { get; set; }
    }
}

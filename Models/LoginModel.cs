﻿using playlist_api.Services;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace playlist_api.Models
{
    public class LoginModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(64)]
        public string email_address { get; set; }

        [Required]
        [JsonConverter(typeof(DefaultConverter))]
        [MaxLength(64)]
        public string password { get; set; }
    }
}

﻿using playlist_api.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace playlist_api.Models
{
    public class MessageModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int contact_message_index { get; set; }

        public Guid account_id { get; set; }

        [Required]
        public string subject { get; set; }

        public string reply_email { get; set; }

        [Required]
        public string body { get; set; }

        [Required]
        public DateTime created { get; set; } = DateTime.UtcNow;
    }
}

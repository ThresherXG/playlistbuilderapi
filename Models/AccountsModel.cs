﻿using playlist_api.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace playlist_api.Models
{
    public class AccountsModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int account_index { get; set; }

        [Required]
        public Guid account_id { get; set; } = Guid.NewGuid();

        [Required]
        [MaxLength(64)]
        public string user_name { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(64)]
        public string email_address { get; set; }

        [Required]
        [JsonConverter(typeof(DefaultConverter))]
        [MinLength(8), MaxLength(64)]
        public string password { get; set; }

        public bool spotify_link { get; set; } = false;

        [Required]
        public DateTime created { get; set; } = DateTime.UtcNow;
    }
}

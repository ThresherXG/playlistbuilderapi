﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace playlist_api.Models
{
    public class TracksModel
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tracks_index { get; set; }

        [Required]
        public Guid track_id { get; set; } = Guid.NewGuid();

        [Required]
        [MaxLength(250)]
        public string title { get; set; }

        [MaxLength(250)]
        public string album { get; set; }

        [MaxLength(250)]
        public string artist { get; set; }

        [Required]
        [MaxLength(250)]
        public string source { get; set; }

        [Required]
        [MaxLength(250)]
        public string source_name { get; set; }

        [MaxLength(250)]
        public string source_url{ get; set; }

        public string spotify_track_id { get; set; }

        public string spotify_uri { get; set; }

        [Required]
        public Guid playlist_id { get; set; }

        [Required]
        public DateTime created { get; set; } = DateTime.UtcNow;
    }
}

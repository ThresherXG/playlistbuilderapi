﻿using System;
using System.ComponentModel.DataAnnotations;

namespace playlist_api.Models.Pandora
{
    public class PandoraPayloadModel
    {
        [Required]
        public Guid playlist_id { get; set; }
        public string pandora_playlist_url { get; set; }
        public string pandora_station_url { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Models
{
    public class PandoraAuthModel
    { 
        public class Rootobject
        {
            public string authToken { get; set; }
            public string listenerId { get; set; }
            public bool explicitContentFilterEnabled { get; set; }
            public string gender { get; set; }
            public bool profilePrivate { get; set; }
            public bool emailOptOut { get; set; }
            public bool artistPromoEmailsEnabled { get; set; }
            public bool artistAudioMessagesEnabled { get; set; }
            public bool isNew { get; set; }
            public Config config { get; set; }
            public string listenerToken { get; set; }
            public string publicLid { get; set; }
            public bool highQualityStreamingEnabled { get; set; }
            public int stationCount { get; set; }
            public string placeholderProfileImageUrl { get; set; }
            public bool autoplayEnabled { get; set; }
            public string kruxToken { get; set; }
            public string webClientVersion { get; set; }
            public Adkv adkv { get; set; }
            public string premiumAccessAdUrl { get; set; }
            public string premiumAccessNoAvailsAdUrl { get; set; }
            public string smartConversionAdUrl { get; set; }
            public bool smartConversionDisabled { get; set; }
            public string staticAdTargeting { get; set; }
        }

        public class Config
        {
            public string branding { get; set; }
            public int dailySkipLimit { get; set; }
            public int stationSkipLimit { get; set; }
            public int inactivityTimeout { get; set; }
            public object[] experiments { get; set; }
            public string[] flags { get; set; }
        }

        public class Adkv
        {
            public string aa { get; set; }
            public string zip { get; set; }
            public string st { get; set; }
            public string hours { get; set; }
            public string prg { get; set; }
            public string ag { get; set; }
            public string hhi { get; set; }
            public string l { get; set; }
            public string clean { get; set; }
            public string co { get; set; }
            public string hisp { get; set; }
            public string et { get; set; }
            public string comped { get; set; }
            public string fam { get; set; }
            public string dse { get; set; }
            public string dma { get; set; }
            public string msa { get; set; }
            public string fb { get; set; }
            public string gnd { get; set; }
            public string iat { get; set; }
        }

    }
}

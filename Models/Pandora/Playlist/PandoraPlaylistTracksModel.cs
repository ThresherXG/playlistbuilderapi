﻿
namespace playlist_api.Models.Pandora
{
    public class PandoraPlaylistTracksModel
    {
        public class Rootobject
        {
            public string name { get; set; }
            public string sortableName { get; set; }
            public int duration { get; set; }
            public int trackNumber { get; set; }
            public int volumeNumber { get; set; }
            public Icon icon { get; set; }
            public Rightsinfo rightsInfo { get; set; }
            public string albumId { get; set; }
            public string albumName { get; set; }
            public string artistId { get; set; }
            public string artistName { get; set; }
            public string explicitness { get; set; }
            public string shareableUrlPath { get; set; }
            public bool hasRadio { get; set; }
            public bool visible { get; set; }
            public long modificationTime { get; set; }
            public string slugPlusPandoraId { get; set; }
            public string stationFactoryId { get; set; }
            public string isrc { get; set; }
            public string pandoraId { get; set; }
            public string type { get; set; }
            public string scope { get; set; }
        }

        public class Icon
        {
            public string dominantColor { get; set; }
            public string thorId { get; set; }
            public string artId { get; set; }
            public string artUrl { get; set; }
        }

        public class Rightsinfo
        {
            public bool hasInteractive { get; set; }
            public bool hasOffline { get; set; }
            public bool hasNonInteractive { get; set; }
            public bool hasStatutory { get; set; }
            public bool hasRadioRights { get; set; }
            public long expirationTime { get; set; }
        }

    }
}

﻿
namespace playlist_api.Models.Pandora
{
    public class PandoraPlaylistPostModel
    {
        public class Rootobject
        {
            public Request request { get; set; }
        }
        public class Request
        {
            public string pandoraId { get; set; }
            public int playlistVersion { get; set; } = 0;
            public int offset { get; set; }
            public int limit { get; } = 99;
            public int annotationLimit { get; } = 99;
            public string[] allowedTypes { get; } = new[] { "TR" };
            public string bypassPrivacyRules { get; } = "true";
        }
    }
}

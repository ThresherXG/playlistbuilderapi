﻿
namespace playlist_api.Models.Pandora
{
    public class PandoraPlaylistModel
    {
       public class Rootobject
        {
            public bool notModified { get; set; }
            public Track[] tracks { get; set; }
            public int offset { get; set; }
            public dynamic annotations { get; set; }
            public string pandoraId { get; set; }
            public int version { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public long timeCreated { get; set; }
            public bool isPrivate { get; set; }
            public bool secret { get; set; }
            public string linkedType { get; set; }
            public string linkedSourceId { get; set; }
            public int totalTracks { get; set; }
            public string shareableUrlPath { get; set; }
            public string thorLayers { get; set; }
            public int duration { get; set; }
            public bool unlocked { get; set; }
            public long timeLastUpdated { get; set; }
            public Viewerinfo viewerInfo { get; set; }
            public bool autogenForListener { get; set; }
            public Listeneridinfo listenerIdInfo { get; set; }
            public string[] includedTrackTypes { get; set; }
            public long timeLastRefreshed { get; set; }
            public bool allowFeedback { get; set; }
            public bool collectible { get; set; }
            public int listenerId { get; set; }
            public string listenerPandoraId { get; set; }
            public string listenerIdToken { get; set; }
        }

        public class Viewerinfo
        {
            public bool editable { get; set; }
        }

        public class Listeneridinfo
        {
            public int listenerId { get; set; }
            public string listenerPandoraId { get; set; }
            public string listenerIdToken { get; set; }
        }

        public class Track
        {
            public int itemId { get; set; }
            public string trackPandoraId { get; set; }
            public long addedTimestamp { get; set; }
            public int duration { get; set; }
        }
    }
}

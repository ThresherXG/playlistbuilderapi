﻿
namespace playlist_api.Models.Pandora
{
    public class PandoraFeedbackPostModel
    {  
        public int pageSize { get; set; }
        public bool positive { get; } = true;
        public int startIndex { get; set; }   
        public string stationId { get; set; }
    }
}

﻿using System;

namespace playlist_api.Models.Pandora
{
    public class PandoraStationFeedbackModel
    {
        public class Rootobject
        {
            public int total { get; set; }
            public Feedback[] feedback { get; set; }
        }

        public class Feedback
        {
            public string feedbackId { get; set; }
            public DateTime feedbackDateCreated { get; set; }
            public bool isPositive { get; set; }
            public string stationId { get; set; }
            public string stationName { get; set; }
            public string stationType { get; set; }
            public string musicId { get; set; }
            public string pandoraId { get; set; }
            public string songTitle { get; set; }
            public string albumTitle { get; set; }
            public string artistName { get; set; }
            public string artistSeoToken { get; set; }
            public string artistDetailUrl { get; set; }
            public string trackSeoToken { get; set; }
            public string trackDetailUrl { get; set; }
            public string albumSeoToken { get; set; }
            public int trackNum { get; set; }
            public int trackLength { get; set; }
            public Albumart[] albumArt { get; set; }
            public string albumDominantColor { get; set; }
            public string trackIdentity { get; set; }
        }

        public class Albumart
        {
            public string url { get; set; }
            public int size { get; set; }
        }

    }
}

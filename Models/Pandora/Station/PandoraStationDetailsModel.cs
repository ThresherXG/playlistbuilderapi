﻿using System;

namespace playlist_api.Models.Pandora
{
    public class PandoraStationDetailsModel
    { 
        public class Rootobject
        {
            public Seed[] seeds { get; set; }
            public int positiveFeedbackCount { get; set; }
            public int negativeFeedbackCount { get; set; }
            public string stationId { get; set; }
            public string stationFactoryPandoraId { get; set; }
            public string pandoraId { get; set; }
            public string name { get; set; }
            public Art2[] art { get; set; }
            public DateTime dateCreated { get; set; }
            public DateTime lastPlayed { get; set; }
            public int totalPlayTime { get; set; }
            public bool isNew { get; set; }
            public bool allowDelete { get; set; }
            public bool allowRename { get; set; }
            public bool allowEditDescription { get; set; }
            public bool allowAddSeed { get; set; }
            public bool isShared { get; set; }
            public bool isTransformAllowed { get; set; }
            public bool isOnDemandEditorialStation { get; set; }
            public bool isAdvertiserStation { get; set; }
            public bool canShuffleStation { get; set; }
            public bool canAutoshare { get; set; }
            public string advertisingKey { get; set; }
            public bool isThumbprint { get; set; }
            public bool isShuffle { get; set; }
            public object[] genre { get; set; }
            public string genreSponsorship { get; set; }
            public Initialseed initialSeed { get; set; }
            public Adkv adkv { get; set; }
            public string creatorWebname { get; set; }
            public string artId { get; set; }
            public string dominantColor { get; set; }
            public int listenerId { get; set; }
            public bool deleted { get; set; }
            public string stationType { get; set; }
            public DateTime timeAdded { get; set; }
            public DateTime lastUpdated { get; set; }
            public bool hasTakeoverModes { get; set; }
            public bool hasCuratedModes { get; set; }
            public Modes modes { get; set; }
        }

        public class Initialseed
        {
            public string musicId { get; set; }
            public string pandoraId { get; set; }
            public Artist artist { get; set; }
            public int listenerCount { get; set; }
            public string seoToken { get; set; }
            public Art[] art { get; set; }
        }

        public class Artist
        {
            public string artistName { get; set; }
            public bool isComposer { get; set; }
            public bool isComedy { get; set; }
            public string artistDetailUrl { get; set; }
        }

        public class Art
        {
            public string url { get; set; }
            public int size { get; set; }
        }

        public class Adkv
        {
            public string artist { get; set; }
            public string genre { get; set; }
            public string clean { get; set; }
            public string gcat { get; set; }
        }

        public class Modes
        {
            public object[] takeoverModes { get; set; }
            public string takeoverModesHeader { get; set; }
            public object[] algorithmicModes { get; set; }
            public string algorithmicModesHeader { get; set; }
            public Annotations annotations { get; set; }
        }

        public class Annotations
        {
        }

        public class Seed
        {
            public string musicId { get; set; }
            public string pandoraId { get; set; }
            public Song song { get; set; }
            public string seoToken { get; set; }
            public Art1[] art { get; set; }
            public Artist1 artist { get; set; }
            public int listenerCount { get; set; }
        }

        public class Song
        {
            public string albumTitle { get; set; }
            public string artistSummary { get; set; }
            public string artistMusicId { get; set; }
            public string artistPandoraId { get; set; }
            public string songTitle { get; set; }
            public bool isComedy { get; set; }
            public string songDetailUrl { get; set; }
        }

        public class Artist1
        {
            public string artistName { get; set; }
            public bool isComposer { get; set; }
            public bool isComedy { get; set; }
            public string artistDetailUrl { get; set; }
        }

        public class Art1
        {
            public string url { get; set; }
            public int size { get; set; }
        }

        public class Art2
        {
            public string url { get; set; }
            public int size { get; set; }
        }
    }
}

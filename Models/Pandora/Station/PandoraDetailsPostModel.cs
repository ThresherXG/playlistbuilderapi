﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Models.Pandora
{
    public class PandoraDetailsPostModel
    {
        public string stationId { get; set; }
        public bool isCurrentStation { get; } = false;
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace playlist_api.Models
{
    public class PlaylistModel
    {

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int playlist_index { get; set;  }

        [Required]
        public Guid playlist_id { get; set; } = Guid.NewGuid();

        [Required]
        [MaxLength(100)]
        public string playlist_name { get; set; }

        public Guid account_id { get; set; }

        
        public bool spotify_created { get; set; } = false;

        [Required]
        public DateTime created { get; set; } = DateTime.UtcNow;
    }
}

﻿using AutoMapper;
using SpotifyAPI.Web;
using playlist_api.DTOs;
using playlist_api.Models;
using playlist_api.Models.Pandora;
using playlist_api.Models.Spotify;

namespace playlist_api.Profiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            //Source -> Target
            CreateMap<AccountsModel, UserProfileDTO>();

            CreateMap<PlaylistModel, PlaylistDTO>();

            CreateMap<TracksModel, TracksDTO>();

            CreateMap<TracksModel, SpotifyTracksModel>();

            CreateMap<FullTrack, SpotifyTracksModel>()
                .ForMember(dest => dest.title, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.album, opt => opt.MapFrom(src => src.Album.Name))
                .ForMember(dest => dest.artist, opt => opt.MapFrom(src => src.Artists[0].Name))
                .ForMember(dest => dest.spotify_uri, opt => opt.MapFrom(src => src.Uri))
                .ForMember(dest => dest.spotify_track_id, opt => opt.MapFrom(src => src.Id));


            CreateMap<PandoraStationFeedbackModel.Feedback, TracksModel>()
                .ForMember(dest => dest.title, opt => opt.MapFrom(src => src.songTitle))
                .ForMember(dest => dest.album, opt => opt.MapFrom(src => src.albumTitle))
                .ForMember(dest => dest.artist, opt => opt.MapFrom(src => src.artistName))
                .ForMember(dest => dest.source_name, opt => opt.MapFrom(src => src.stationName))
                .ForMember(dest => dest.source, opt => opt.MapFrom(src => "Pandora"))
                .ForMember(dest => dest.playlist_id, opt => opt.Ignore())
                .ForMember(dest => dest.source_url, opt => opt.Ignore());

           CreateMap<PandoraPlaylistTracksModel.Rootobject, TracksModel>()
               .ForMember(dest => dest.title, opt => opt.MapFrom(src => src.name))
               .ForMember(dest => dest.album, opt => opt.MapFrom(src => src.albumName))
               .ForMember(dest => dest.artist, opt => opt.MapFrom(src => src.artistName))
               .ForMember(dest => dest.source, opt => opt.MapFrom(src => "Pandora"))
               .ForMember(dest => dest.playlist_id, opt => opt.Ignore())
               .ForMember(dest => dest.source_name, opt => opt.Ignore())
               .ForMember(dest => dest.source_url, opt => opt.Ignore());
        }
    }
}

﻿using playlist_api.Models;
using playlist_api.Models.Spotify;
using SpotifyAPI.Web;
using System;

namespace playlist_api.Profiles
{
    public class CustomMapperProfile
    {
        public SpotifyAccountsModel mapSpotifyAccountProfile(Guid account_id, PrivateUser _spotifyUser, AuthorizationCodeTokenResponse response)
        {
            SpotifyAccountsModel _spotifyAccount = new SpotifyAccountsModel
            {
                account_id = account_id,
                spotify_account_id = _spotifyUser.Id,
                spotify_display_name = _spotifyUser.DisplayName,
                spotify_email = _spotifyUser.Email,
                spotify_account_url = _spotifyUser.ExternalUrls["spotify"],
                access_token = response.AccessToken,
                refresh_token = response.RefreshToken,
                expires_in = response.ExpiresIn,
                token_created = response.CreatedAt,
                country = _spotifyUser.Country
            };

            return _spotifyAccount;
        }

        public TracksModel mapSpotifyTrackJSONToTrack(SpotifyAPITracksModel.Rootobject spotifyJSON, string linkToTrack, Guid playlist_id)
        {
            var track = new TracksModel
            {
                title = spotifyJSON.name,
                album = spotifyJSON.album.name,
                artist = spotifyJSON.artists[0].name,
                source = "Spotify",
                source_name = "Spotify Track",
                source_url = linkToTrack,
                spotify_track_id = spotifyJSON.id,
                spotify_uri = spotifyJSON.uri,
                playlist_id = playlist_id,
            };

            return track;
        }

        public TracksModel mapSpotifyPlaylistJSONToTrack(SpotifyPlaylistModel.Item spotifyJSON, string linkToPlaylist, string nameOfPlaylist, Guid playlist_id)
        {
            var track = new TracksModel
            {
                title = spotifyJSON.track.name,
                album = spotifyJSON.track.album.name,
                artist = spotifyJSON.track.artists[0].name,
                source = "Spotify",
                source_name = nameOfPlaylist,
                source_url = linkToPlaylist,
                spotify_track_id = spotifyJSON.track.id,
                spotify_uri = spotifyJSON.track.uri,
                playlist_id = playlist_id,
            };

            return track;
        }

        public TracksModel mapSpotifyAlbumJSONToTrack(SpotifyAlbumModel.Item spotifyJSON, string linkToAlbum, string nameOfAlbum, Guid playlist_id)
        {
            var track = new TracksModel
            {
                title = spotifyJSON.name,
                album = nameOfAlbum,
                artist = spotifyJSON.artists[0].name,
                source = "Spotify",
                source_name = nameOfAlbum,
                source_url = linkToAlbum,
                spotify_track_id = spotifyJSON.id,
                spotify_uri = spotifyJSON.uri,
                playlist_id = playlist_id,
            };

            return track;
        }

        public TracksModel mapSpotifyPageationJSONToTrack(SpotifyPlaylistPageationModel.Item spotifyJSON, string linkToPlaylist, string nameOfPlaylist, Guid playlist_id)
        {
            var track = new TracksModel
            {
                title = spotifyJSON.track.name,
                album = spotifyJSON.track.album.name,
                artist = spotifyJSON.track.artists[0].name,
                source = "Spotify",
                source_name = nameOfPlaylist,
                source_url = linkToPlaylist,
                spotify_track_id = spotifyJSON.track.id,
                spotify_uri = spotifyJSON.track.uri,
                playlist_id = playlist_id,
            };

            return track;
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using playlist_api.Models.Spotify;

#nullable disable

namespace playlist_api.Models
{
    public partial class PlaylistBuilderContext : DbContext
    {
        public PlaylistBuilderContext(DbContextOptions<PlaylistBuilderContext> options) : base(options)
        {

        }

        //tables
        public DbSet<AccountsModel> accounts { get; set; }
        public DbSet<SpotifyAccountsModel> spotify_accounts { get; set; }
        public DbSet<PlaylistModel> playlists { get; set; }
        public DbSet<TracksModel> tracks { get; set; }
        public DbSet<SpotifyTracksModel> spotify_tracks { get; set; }
        public DbSet<MessageModel> messages { get; set; }

        //ef logging
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        internal Task SaveChangesAsyc()
        {
            throw new NotImplementedException();
        }
    }
}

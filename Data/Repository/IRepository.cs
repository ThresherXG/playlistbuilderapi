﻿using System;
using System.Threading.Tasks;

namespace playlist_api.Data
{
    public interface IRepository<T>
    {
        Task Create(T entity);
        Task Update(T entity);
        Task Delete(T entity);
    }
}

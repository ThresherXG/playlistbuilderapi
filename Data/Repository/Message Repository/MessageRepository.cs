﻿using playlist_api.Data.Repository.Message_Repository;
using playlist_api.Models;

namespace playlist_api.Data.Repository
{
    public class MessageRepository : Repository<MessageModel>, IMessageRepository
    {
        private readonly PlaylistBuilderContext _context;

        public MessageRepository(PlaylistBuilderContext context) : base(context)
        {
            _context = context;
        }

    }
}

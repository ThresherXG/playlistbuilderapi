﻿using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Data.Repository.Message_Repository
{
    public interface IMessageRepository : IRepository<MessageModel>
    {
    }
}

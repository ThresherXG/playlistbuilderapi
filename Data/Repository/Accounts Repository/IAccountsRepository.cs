﻿using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Data
{
    public interface IAccountsRepository : IRepository<AccountsModel>
    {
        Task<AccountsModel> GetAccountById(Guid account_id);
        Task<AccountsModel> GetAccountByEmail(string email_address);
        Task<AccountsModel> GetAccountByUserName(string user_name);
    }
}

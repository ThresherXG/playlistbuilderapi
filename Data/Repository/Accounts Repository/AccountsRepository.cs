﻿using Microsoft.EntityFrameworkCore;
using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Data
{
    public class AccountsRepository : Repository<AccountsModel>, IAccountsRepository
    {
        private readonly PlaylistBuilderContext _context;
        public AccountsRepository(PlaylistBuilderContext context) : base(context)
        {
            _context = context;        
        }

        public async Task<AccountsModel> GetAccountById(Guid account_id)
        {
            return await _context.accounts.FirstOrDefaultAsync(x => x.account_id == account_id);
        }

        public async Task<AccountsModel> GetAccountByEmail(string email_address)
        {
            return await _context.accounts.FirstOrDefaultAsync(x => x.email_address == email_address);
        }

        public async Task<AccountsModel> GetAccountByUserName(string user_name)
        {
            return await _context.accounts.FirstOrDefaultAsync(x => x.user_name == user_name);
        }

    }
}

﻿using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Data
{
    public interface ISpotifyAccountsRepository : IRepository<SpotifyAccountsModel>
    {
        Task<SpotifyAccountsModel> GetSpotfiyAccountById(Guid account_id);
    }
}

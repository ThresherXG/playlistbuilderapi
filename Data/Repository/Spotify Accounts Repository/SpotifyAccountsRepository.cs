﻿using Microsoft.EntityFrameworkCore;
using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Data
{
    public class SpotifyAccountsRepository : Repository<SpotifyAccountsModel>, ISpotifyAccountsRepository
    {
        private readonly PlaylistBuilderContext _context;
        public SpotifyAccountsRepository(PlaylistBuilderContext context) : base(context)
        {
            _context = context;
        }

        public async Task<SpotifyAccountsModel> GetSpotfiyAccountById(Guid account_id)
        {
            return await _context.spotify_accounts.FirstOrDefaultAsync(x => x.account_id == account_id);
        }
    }
}

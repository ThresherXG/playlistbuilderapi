﻿using Microsoft.EntityFrameworkCore;
using playlist_api.Models;
using System;
using System.Threading.Tasks;

namespace playlist_api.Data
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly PlaylistBuilderContext _context;
        private DbSet<T> entities;

        public Repository(PlaylistBuilderContext context)
        {
            entities = context.Set<T>();
            _context = context;         
        }

        public async Task Create(T entity)
        {
             entities.Add(entity);
             await _context.SaveChangesAsync();
        }

        public async Task Delete(T entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(T entity)
        {
            entities.Update(entity);
            await _context.SaveChangesAsync();
        }
    }
}

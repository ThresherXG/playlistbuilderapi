﻿using playlist_api.Models;
using playlist_api.Models.Spotify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Data.Repository.Tracks_Repository
{
    public interface ITracksRepository : IRepository<TracksModel>
    {
        Task<List<TracksModel>> getAllTracksByPlaylistId(Guid playlist_id);
        Task<List<TracksModel>> getAllTracksByPlaylistIdWhereSpotifyIdisNull(Guid playlist_id);
        Task<List<string>> getAllTrackUrisByPlaylistId(Guid playlist_id);
        Task<TracksModel> getTrackByTrackId(Guid track_id);
        Task<int> countTracksByPlaylistId(Guid playlist_id);
        Task bulkCreateTracks(List<TracksModel> allTracks);
        Task bulkUpdateTracks(List<TracksModel> allTracks);
        Task bulkDeleteTracks(TracksModel[] allTracks);
        Task deleteAllTracksByPlaylistId(Guid playlist_id);
        Task deleteDuplicatePlaylistTracks(Guid playlist_id);
        Task bulkUpdateSpotifyIdsFromSpotifyTracks(Guid playlist_id);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Npgsql.Bulk;
using playlist_api.Services;
using playlist_api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace playlist_api.Data.Repository.Tracks_Repository
{
    public class TracksRepository : Repository<TracksModel>, ITracksRepository
    {
        private readonly PlaylistBuilderContext _context;

        public TracksRepository(PlaylistBuilderContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<TracksModel>> getAllTracksByPlaylistId(Guid playlist_id)
        {
            var tracks = await _context.tracks.Where(x => x.playlist_id == playlist_id).ToListAsync();

            return tracks;
        }

        public async Task<List<TracksModel>> getAllTracksByPlaylistIdWhereSpotifyIdisNull(Guid playlist_id)
        {
            var tracks = await _context.tracks.Where(x => x.playlist_id == playlist_id && x.spotify_track_id == null && x.spotify_uri == null).ToListAsync();

            return tracks;
        }

        public async Task<List<string>> getAllTrackUrisByPlaylistId(Guid playlist_id)
        {
            var trackUris = await _context.tracks.Where(x => x.playlist_id == playlist_id).Select(x => x.spotify_uri).ToListAsync();

            return trackUris;
        }

        public async Task<int> countTracksByPlaylistId(Guid playlist_id)
        {
            var countOfTracks = await _context.tracks.Where(x => x.playlist_id == playlist_id).CountAsync();

            return countOfTracks;
        }

        public async Task<TracksModel> getTrackByTrackId(Guid track_id)
        {
            return await _context.tracks.FirstOrDefaultAsync(x => x.track_id == track_id);
        }

        public async Task bulkCreateTracks(List<TracksModel> allTracks)
        {
            var bulkUploader = new NpgsqlBulkUploader(_context);

            await bulkUploader.InsertAsync(allTracks);
        }

        public async Task bulkUpdateTracks(List<TracksModel> allTracks)
        {
            var bulkUploader = new NpgsqlBulkUploader(_context);

            await bulkUploader.UpdateAsync(allTracks);
        }


        public async Task bulkDeleteTracks(TracksModel[] allTracks)
        {
            _context.RemoveRange(allTracks);

            await _context.SaveChangesAsync();
        }

        public async Task deleteAllTracksByPlaylistId(Guid playlist_id)
        {
            _context.RemoveRange(_context.tracks.Where(x => x.playlist_id == playlist_id));

            await _context.SaveChangesAsync();
        }

        public async Task deleteDuplicatePlaylistTracks(Guid playlist_id)
        {
            await _context.Database.ExecuteSqlRawAsync($"DELETE FROM public.tracks WHERE ctid IN (SELECT ctid FROM(SELECT *, ctid, row_number() OVER (PARTITION BY title, artist, playlist_id ORDER BY ctid) FROM public.tracks WHERE playlist_id = '{playlist_id}')s WHERE row_number >= 2 AND playlist_id = '{playlist_id}')");
        }

        public async Task bulkUpdateSpotifyIdsFromSpotifyTracks(Guid playlist_id)
        {
            await _context.Database.ExecuteSqlRawAsync($"UPDATE public.tracks SET spotify_track_id = public.spotify_tracks.spotify_track_id, spotify_uri = public.spotify_tracks.spotify_uri FROM public.spotify_tracks WHERE public.spotify_tracks.title = tracks.title AND public.spotify_tracks.artist = tracks.artist AND public.spotify_tracks.album = tracks.album AND public.tracks.spotify_track_id IS NULL AND public.tracks.playlist_id = '{playlist_id}'");
        }
    }  
}

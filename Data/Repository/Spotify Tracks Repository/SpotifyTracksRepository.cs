﻿using Microsoft.EntityFrameworkCore;
using Npgsql.Bulk;
using playlist_api.Services;
using playlist_api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using playlist_api.Models.Spotify;

namespace playlist_api.Data.Repository.Tracks_Repository
{
    public class SpotifyTracksRepository : Repository<SpotifyTracksModel>, ISpotifyTracksRepository
    {
        private readonly PlaylistBuilderContext _context;

        public SpotifyTracksRepository(PlaylistBuilderContext context) : base(context)
        {
            _context = context;
        }

        public async Task<SpotifyTracksModel> getSpotifyTrackByTitle(string title)
        {
            return await _context.spotify_tracks.FirstOrDefaultAsync(x => x.title == title);
        }

        public async Task bulkCreateSpotifyTracks(List<SpotifyTracksModel> allSpotifyTracks)
        {
            var bulkUploader = new NpgsqlBulkUploader(_context);

            await bulkUploader.InsertAsync(allSpotifyTracks);
        }
    }  
}

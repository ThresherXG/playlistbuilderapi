﻿using playlist_api.Models;
using playlist_api.Models.Spotify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Data.Repository.Tracks_Repository
{
    public interface ISpotifyTracksRepository : IRepository<SpotifyTracksModel>
    {
        Task<SpotifyTracksModel> getSpotifyTrackByTitle(string title);
        Task bulkCreateSpotifyTracks(List<SpotifyTracksModel> allSpotifyTracks);
    }
}

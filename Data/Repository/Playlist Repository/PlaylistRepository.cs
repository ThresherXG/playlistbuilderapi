﻿using Microsoft.EntityFrameworkCore;
using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Data.Repository.Playlist_Repository
{
    public class PlaylistRepository : Repository<PlaylistModel>, IPlaylistRepository
    {
        private readonly PlaylistBuilderContext _context;

        public PlaylistRepository(PlaylistBuilderContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<PlaylistModel>> getAllPlaylistsByUserId(Guid user_id)
        {
            return await _context.playlists.Where(x => x.account_id == user_id).OrderBy(x => x.created).ToListAsync();       
        }

        public async Task<PlaylistModel> getPlaylistById(Guid playlist_id)
        {
            return await _context.playlists.FirstOrDefaultAsync(x => x.playlist_id == playlist_id);
        }
    }
}

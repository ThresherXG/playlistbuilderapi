﻿using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace playlist_api.Data.Repository.Playlist_Repository
{
    public interface IPlaylistRepository : IRepository<PlaylistModel>
    {
        Task<IEnumerable<PlaylistModel>> getAllPlaylistsByUserId(Guid user_id);
        Task<PlaylistModel> getPlaylistById(Guid playlist_id);
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace playlist_api.DTOs
{
    public class PlaylistDTO
    {
        public string playlist_id { get; set; }

        public string playlist_name { get; set; }

        public bool spotify_created { get; set; }
    }
}

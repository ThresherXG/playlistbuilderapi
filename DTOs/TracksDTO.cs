﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.DTOs
{
    public class TracksDTO
    {
        public string tracks_index { get; set; }

        public string track_id { get; set; }

        public string title { get; set; }

        public string album { get; set; }

        public string artist { get; set; }

        public string source { get; set; }

        public string source_name { get; set; }

        public string source_url { get; set; }

        public bool spotify_exists { get; set; }

        public string playlist_id { get; set; }

        public DateTime created { get; set; }
    }
}

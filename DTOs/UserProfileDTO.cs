﻿using System.ComponentModel.DataAnnotations;

namespace playlist_api.DTOs
{
    public class UserProfileDTO
    {
        public string user_name { get; set; }

        public string spotify_display_name { get; set; }

        public bool spotify_link { get; set; }

        public string spotify_account_url { get; set; }
    }
}

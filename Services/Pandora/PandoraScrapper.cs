﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using playlist_api.Models;
using playlist_api.Models.Pandora;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace playlist_api.Services.Pandora
{
    public class PandoraScrapper : IPandoraScrapper
    {
        private readonly IHttpClientFactory _factory;
        private readonly IMapper _mapper;
        private readonly ILogger<PandoraScrapper> _logger;
        private readonly IHelper _helper;
        public PandoraScrapper(IHttpClientFactory factory, IMapper mapper, ILogger<PandoraScrapper> logger, IHelper helper)
        {
            _factory = factory;
            _mapper = mapper;
            _logger = logger;
            _helper = helper;
        }

        public async Task<Dictionary<string, string>> refreshPandoraTokens()
        {
            try 
            {
                var pandoraClient = _factory.CreateClient("Pandora");

                var response = await pandoraClient.GetAsync("");

                var cookies = response.Headers.SingleOrDefault(header => header.Key == "Set-Cookie").Value;

                var csrfCookie = cookies.SingleOrDefault(cookie => cookie.Contains("csrf"));

                var csrfToken = csrfCookie.Substring(10, 16);

                pandoraClient.DefaultRequestHeaders.Add("cookie", "csrftoken=" + csrfToken);
                pandoraClient.DefaultRequestHeaders.Add("x-csrftoken", csrfToken);

                var stringContent = new StringContent(string.Empty);
                stringContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

                var authResponse = await pandoraClient.PostAsync("/api/v1/auth/anonymousLogin", stringContent);

                var text = await authResponse.Content.ReadAsStringAsync();

                var authToken = JsonSerializer.Deserialize<PandoraAuthModel.Rootobject>(text);

                var pandoraAuthDictionary = new Dictionary<string, string>
                {
                    { "authToken", authToken.authToken },
                    { "csrfToken", csrfToken }
                };

                return pandoraAuthDictionary;
            }
            catch (Exception e) 
            {
                _logger.LogInformation(e.Message);
            }

            return new Dictionary<string, string>();
        }

        public async Task<List<TracksModel>> scrapePandoraStation(PandoraPayloadModel pandoraStation, Dictionary<string, string> pandoraAuthDict)
        {
            try
            {
                string stationId = _helper.parseUrl(pandoraStation.pandora_station_url);

                var pandoraClient = _factory.CreateClient("Pandora");

                pandoraClient.DefaultRequestHeaders.Add("cookie", "csrftoken=" + pandoraAuthDict["csrfToken"]);
                pandoraClient.DefaultRequestHeaders.Add("x-authtoken", pandoraAuthDict["authToken"]);
                pandoraClient.DefaultRequestHeaders.Add("x-csrftoken", pandoraAuthDict["csrfToken"]);

                var detailsPayload = new PandoraDetailsPostModel
                {
                    stationId = stationId
                };

                string jsonDetailsPayload = JsonSerializer.Serialize(detailsPayload);

                var content = new StringContent(jsonDetailsPayload, Encoding.UTF8, "application/json");

                var detailsResponse = await pandoraClient.PostAsync("/api/v1/station/getStationDetails", content);

                var detailsBody = await detailsResponse.Content.ReadAsStringAsync();

                var detailsBodyJSON = JsonSerializer.Deserialize<PandoraStationDetailsModel.Rootobject>(detailsBody);

                if(detailsBodyJSON.positiveFeedbackCount == 0) 
                {
                    return new List<TracksModel>();
                }

                var getStationLoops = Math.Ceiling(Decimal.Divide(detailsBodyJSON.positiveFeedbackCount, 99));

                var feedbackPayload = new PandoraFeedbackPostModel
                {
                    stationId = stationId,
                    pageSize = 99,
                    startIndex = 0
                };

                var allStationTracks = new List<List<TracksModel>>();

                for (int i = 0; i < getStationLoops; i++)
                {
                    var stationTracks = new List<TracksModel>();

                    string jsonFeedbackPayload = JsonSerializer.Serialize(feedbackPayload);

                    var feedbackContent = new StringContent(jsonFeedbackPayload, Encoding.UTF8, "application/json");

                    var feedbackResponse = await pandoraClient.PostAsync("/api/v1/station/getStationFeedback", feedbackContent);

                    var feedbackBody = await feedbackResponse.Content.ReadAsStringAsync();

                    var feedbackBodyJson = JsonSerializer.Deserialize<PandoraStationFeedbackModel.Rootobject>(feedbackBody);

                    foreach(var feedback in feedbackBodyJson.feedback)
                    {
                        var mappedFeedback = _mapper.Map<TracksModel>(feedback);

                        mappedFeedback.playlist_id = pandoraStation.playlist_id;
                        mappedFeedback.source_url = pandoraStation.pandora_station_url;

                        stationTracks.Add(mappedFeedback);
                    }

                    allStationTracks.Add(stationTracks);

                    feedbackPayload.startIndex += 98;
                }

                var allStationTracksList = allStationTracks.SelectMany(x => x).ToList();

                return allStationTracksList;
            }
            catch (Exception e)
            {
                _logger.LogInformation(e.Message);
            }

            return new List<TracksModel>();
        }

        public async Task<List<TracksModel>> scrapePandoraPlaylist(PandoraPayloadModel pandoraPlaylist, Dictionary<string, string> pandoraAuthDict) 
        {
            try 
            {
                string playlistId = _helper.parseUrl(pandoraPlaylist.pandora_playlist_url);

                var pandoraClient = _factory.CreateClient("Pandora");

                pandoraClient.DefaultRequestHeaders.Add("cookie", "csrftoken=" + pandoraAuthDict["csrfToken"]);
                pandoraClient.DefaultRequestHeaders.Add("x-authtoken", pandoraAuthDict["authToken"]);
                pandoraClient.DefaultRequestHeaders.Add("x-csrftoken", pandoraAuthDict["csrfToken"]);

                var playlistPayload = new PandoraPlaylistPostModel.Rootobject
                {
                    request = new PandoraPlaylistPostModel.Request
                    {
                        pandoraId = playlistId,
                        offset = 0
                    }
                };

                var allPlaylistTracks = new List<List<TracksModel>>();

                bool playlistNotFull = true;

                int playlistTracksScraped = 0;

                while (playlistNotFull)
                {
                    string jsonPlaylistPayload = JsonSerializer.Serialize(playlistPayload);

                    var content = new StringContent(jsonPlaylistPayload, Encoding.UTF8, "application/json");

                    var playlistResponse = await pandoraClient.PostAsync("/api/v7/playlists/getTracks", content);

                    var playlistBody = await playlistResponse.Content.ReadAsStringAsync();

                    var playlistBodyJSON = JsonSerializer.Deserialize<PandoraPlaylistModel.Rootobject>(playlistBody);

                    var annotationsDict = JsonSerializer.Deserialize<Dictionary<string, object>>(playlistBodyJSON.annotations.ToString());

                    var playlistTracksList = new List<TracksModel>();

                    foreach (KeyValuePair<string, object> entry in annotationsDict)
                    {
                        var annotationObject = JsonSerializer.Deserialize<PandoraPlaylistTracksModel.Rootobject>(entry.Value.ToString());

                        if (annotationObject.type == "TR")
                        {
                            var mappedAnnotation = _mapper.Map<TracksModel>(annotationObject);

                            mappedAnnotation.playlist_id = pandoraPlaylist.playlist_id;
                            mappedAnnotation.source_url = pandoraPlaylist.pandora_playlist_url;
                            mappedAnnotation.source_name = playlistBodyJSON.name;

                            playlistTracksList.Add(mappedAnnotation);
                        }
                    }

                    playlistTracksScraped += playlistTracksList.Count;
                    playlistPayload.request.offset += 99;
                    playlistPayload.request.playlistVersion = playlistBodyJSON.version;

                    if (playlistTracksScraped == playlistBodyJSON.totalTracks)
                    {
                        playlistNotFull = false;
                    }

                    allPlaylistTracks.Add(playlistTracksList);
                }

                var allPlaylistTracksList = allPlaylistTracks.SelectMany(x => x).ToList();

                return allPlaylistTracksList;
            }
            catch(Exception e)
            {
                _logger.LogInformation(e.Message);
            }

            return new List<TracksModel>();
        }
    }
}

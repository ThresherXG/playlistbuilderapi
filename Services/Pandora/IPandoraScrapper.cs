﻿using playlist_api.Models;
using playlist_api.Models.Pandora;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace playlist_api.Services.Pandora
{
    public interface IPandoraScrapper
    {
        Task<Dictionary<string, string>> refreshPandoraTokens();
        Task<List<TracksModel>> scrapePandoraStation(PandoraPayloadModel model, Dictionary<string, string> pandoraAuthDict);

        Task<List<TracksModel>> scrapePandoraPlaylist(PandoraPayloadModel model, Dictionary<string, string> pandoraAuthDict);
    }
}

﻿using Microsoft.AspNetCore.Http;
using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Services.iTunes
{
    public class iTunesProcessor : IiTunesProcessor
    {
        public async Task<List<TracksModel>> processiTunesTXTFile(IFormFileCollection files, string playlist_id)
        {
            var alliTunesTracks = new List<TracksModel>();

            string itunesTXTLine;

            foreach (var formFile in files)
            {
                if(formFile.ContentType != "text/plain") 
                {
                    return alliTunesTracks;
                }

                if (formFile.Length > 0)
                {
                    var streamReader = new StreamReader(formFile.OpenReadStream());

                    string header = streamReader.ReadLine();

                    while (streamReader.Peek() >= 0)
                    {
                        var itunesTrackModel = new TracksModel();

                        itunesTXTLine = await streamReader.ReadLineAsync();

                        string[] delimited_itunesTXTLine = itunesTXTLine.Split('\t');

                        if(delimited_itunesTXTLine.Length != 31)
                        {
                            return new List<TracksModel>();
                        }

                        itunesTrackModel.title = delimited_itunesTXTLine[0];
                        itunesTrackModel.artist = delimited_itunesTXTLine[1];
                        itunesTrackModel.album = delimited_itunesTXTLine[3];
                        itunesTrackModel.playlist_id = Guid.Parse(playlist_id);
                        itunesTrackModel.source = "iTunes";
                        itunesTrackModel.source_name = formFile.FileName;
                        itunesTrackModel.source_url = "";

                        alliTunesTracks.Add(itunesTrackModel);
                    }
                }
            }

            return alliTunesTracks;
        }
    }
}

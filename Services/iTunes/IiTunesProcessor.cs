﻿using Microsoft.AspNetCore.Http;
using playlist_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playlist_api.Services.iTunes
{
    public interface IiTunesProcessor
    {
        Task<List<TracksModel>> processiTunesTXTFile(IFormFileCollection files, string playlist_id);
    }
}

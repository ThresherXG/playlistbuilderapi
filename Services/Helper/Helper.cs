﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using playlist_api.Data;
using playlist_api.Models;
using SpotifyAPI.Web;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace playlist_api.Services
{
    public class Helper : IHelper
    {
        public  string GenerateJSONWebToken(AccountsModel account, IConfiguration _config)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWT:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, account.account_id.ToString())
            };

            var token = new JwtSecurityToken(
              _config["JWT:Issuer"],
              _config["JWT:Issuer"],
              claims,
              expires: DateTime.Now.AddDays(7),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public Guid getUserFromJWT(ClaimsPrincipal user)
        {
            var claims = user.Claims.ToList();

            string user_id = claims[0].Value;

            return Guid.Parse(user_id);
        }

        public string parseUrl(string url) 
        {
            var lastBackSlash = url.LastIndexOf("/");

            string iD = url.Substring(lastBackSlash + 1);

            return iD;

        }
    }
}

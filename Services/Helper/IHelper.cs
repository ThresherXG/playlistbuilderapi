﻿using Microsoft.Extensions.Configuration;
using playlist_api.Data;
using playlist_api.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace playlist_api.Services
{
    public interface IHelper
    {
        string GenerateJSONWebToken(AccountsModel account, IConfiguration _config);
        string parseUrl(string url);
        Guid getUserFromJWT(ClaimsPrincipal user);        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using FuzzySharp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using playlist_api.Data;
using playlist_api.Models;
using playlist_api.Models.Spotify;
using playlist_api.Profiles;
using SpotifyAPI.Web;


namespace playlist_api.Services.Spotify
{

    public class SpotifyProcessor : ISpotifyProcessor
    {
        private readonly IHelper _helper;
        private readonly IMapper _mapper;
        private readonly IHttpClientFactory _factory;
        private readonly CustomMapperProfile _customerMapper;
        private readonly ILogger<SpotifyProcessor> _logger;    

        public SpotifyProcessor(IHelper helper, IHttpClientFactory factory, IMapper mapper, ILogger<SpotifyProcessor> logger, CustomMapperProfile customerMapper)
        {
            _helper = helper;
            _factory = factory;
            _mapper = mapper;
            _logger = logger;
            _customerMapper = customerMapper;
        }

        public async Task<SpotifyReturnModel> scrapeSpotifyPlaylist(SpotifyPayloadModel spotifyPlaylistModel, SpotifyAccountsModel spotifyAccountModel)
        {
            var spotifyClient = _factory.CreateClient("Spotify");

            var spotifyPlaylistId = _helper.parseUrl(spotifyPlaylistModel.spotify_playlist_url);

            spotifyClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", spotifyAccountModel.access_token);

            var allTracks = new List<List<TracksModel>>();

            var allSpotifyTracks = new List<List<SpotifyTracksModel>>();

            try
            {
                var playlistResponse = await spotifyClient.GetAsync("playlists/" + spotifyPlaylistId);

                var playlistBody = await playlistResponse.Content.ReadAsStringAsync();

                var playlistBodyJSON = JsonSerializer.Deserialize<SpotifyPlaylistModel.Rootobject>(playlistBody);

                var next = playlistBodyJSON.tracks.next;

                var nameOfPlaylist = playlistBodyJSON.name;

                var linkToPlaylist = playlistBodyJSON.external_urls.spotify;

                var playlistItems = playlistBodyJSON.tracks.items;

                var listOfTracks = new List<TracksModel>();
                var listOfSpotfiyTracks = new List<TracksModel>();

                foreach (var spotifyTrack in playlistItems)
                {
                    if (spotifyTrack.track != null) 
                    { 
                        var track = _customerMapper.mapSpotifyPlaylistJSONToTrack(spotifyTrack, linkToPlaylist, nameOfPlaylist, spotifyPlaylistModel.playlist_id); 
                        listOfTracks.Add(track);

                        if (track.spotify_track_id != null)
                        {
                            listOfSpotfiyTracks.Add(track);
                        }
                    }          
                }

                var spotifyTracks = _mapper.Map<List<SpotifyTracksModel>>(listOfSpotfiyTracks);

                allSpotifyTracks.Add(spotifyTracks);

                allTracks.Add(listOfTracks);

                if (next != null)
                {
                    bool pageation = true;

                    while (pageation)
                    {
                        var pageationTracks = new List<TracksModel>();
                        var pageationSpotifyTracks = new List<TracksModel>();

                        var pageationUrl = _helper.parseUrl(next);

                        var pageationPlaylistResponse = await spotifyClient.GetAsync("playlists/" + spotifyPlaylistId + "/" + pageationUrl);

                        var pageationPlaylistBody = await pageationPlaylistResponse.Content.ReadAsStringAsync();

                        var pageationPlaylistBodyJSON = JsonSerializer.Deserialize<SpotifyPlaylistPageationModel.Rootobject>(pageationPlaylistBody);

                        var pageationPlaylistItems = pageationPlaylistBodyJSON.items;

                        foreach (var spotifyTrack in pageationPlaylistItems)
                        {
                            var track = _customerMapper.mapSpotifyPageationJSONToTrack(spotifyTrack, linkToPlaylist, nameOfPlaylist, spotifyPlaylistModel.playlist_id);

                            pageationTracks.Add(track);

                            if (track.spotify_track_id != null)
                            {
                                pageationSpotifyTracks.Add(track);
                            }
                        }

                        var mappedPageationSpotifyTracks = _mapper.Map<List<SpotifyTracksModel>>(pageationSpotifyTracks);

                        allSpotifyTracks.Add(mappedPageationSpotifyTracks);
                        allTracks.Add(pageationTracks);

                        if (pageationPlaylistBodyJSON.next != null)
                        {
                            next = pageationPlaylistBodyJSON.next;
                        }
                        else
                        {
                            pageation = false;
                        }
                    }
                }

                var bothTracksLists = new SpotifyReturnModel
                {
                    TracksList = allTracks.SelectMany(x => x).ToList(),
                    SpotifyTracksList = allSpotifyTracks.SelectMany(x => x).ToList()
                };

                return bothTracksLists;
            }
            catch (Exception e)
            {
                _logger.LogInformation(e.Message);
            }

            return new SpotifyReturnModel();
        }

        public async Task<SpotifyReturnModel> scrapeSpotifyAlbum(SpotifyPayloadModel spotifyAlbumModel, SpotifyAccountsModel spotifyProfile)
        {
            var spotifyClient = _factory.CreateClient("Spotify");

            var spotifyAlbumId = _helper.parseUrl(spotifyAlbumModel.spotify_album_url);

            spotifyClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", spotifyProfile.access_token);

            try
            {
                var albumResponse = await spotifyClient.GetAsync("albums/" + spotifyAlbumId);

                var albumBody = await albumResponse.Content.ReadAsStringAsync();

                var albumBodyJSON = JsonSerializer.Deserialize<SpotifyAlbumModel.Rootobject>(albumBody);

                var albumTracks = albumBodyJSON.tracks.items;

                var linkToAlbum = albumBodyJSON.external_urls.spotify;

                var nameOfAlbum = albumBodyJSON.name;

                var listOfTracks = new List<TracksModel>();
                var listOfSpotfiyTracks = new List<SpotifyTracksModel>();

                foreach (var song in albumTracks)
                {
                    var track = _customerMapper.mapSpotifyAlbumJSONToTrack(song, linkToAlbum, nameOfAlbum, spotifyAlbumModel.playlist_id);

                    var mappedSpotifyTrack = _mapper.Map<SpotifyTracksModel>(track);

                    listOfTracks.Add(track);

                    listOfSpotfiyTracks.Add(mappedSpotifyTrack);
                }

                var bothTracksLists = new SpotifyReturnModel
                {
                    TracksList = listOfTracks,
                    SpotifyTracksList = listOfSpotfiyTracks,
                };

                return bothTracksLists;
            }
            catch (Exception e)
            {
                _logger.LogInformation(e.Message);
            }

            return new SpotifyReturnModel();
        }

        public async Task<SpotifyReturnModel> scrapeSpotifyTrack(SpotifyPayloadModel spotifyTracksModel, SpotifyAccountsModel spotifyProfile)
        {
            var spotifyClient = _factory.CreateClient("Spotify");

            var positionOfColon = spotifyTracksModel.spotify_track_url.LastIndexOf(':');

            string spotifyTrackId;

            if (positionOfColon <= 5) 
            {
                spotifyTrackId = _helper.parseUrl(spotifyTracksModel.spotify_track_url);

                var positionOfQuestionMark = spotifyTrackId.LastIndexOf('?');

                if(positionOfQuestionMark != 0) 
                {
                    spotifyTrackId.Substring(positionOfQuestionMark + 1);
                }
            }
            else
            {
                spotifyTrackId = spotifyTracksModel.spotify_track_url.Substring(positionOfColon + 1);
            }
         
            spotifyClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", spotifyProfile.access_token);

            try
            {
                var trackResponse = await spotifyClient.GetAsync("tracks/" + spotifyTrackId);

                var trackBody = await trackResponse.Content.ReadAsStringAsync();

                var tracksBodyJSON = JsonSerializer.Deserialize<SpotifyAPITracksModel.Rootobject>(trackBody);

                var linkToTrack = tracksBodyJSON.external_urls.spotify;

                var track = _customerMapper.mapSpotifyTrackJSONToTrack(tracksBodyJSON, linkToTrack, spotifyTracksModel.playlist_id);

                var mappedSpotifyTrack = _mapper.Map<SpotifyTracksModel>(track);

                var bothTracks = new SpotifyReturnModel
                {
                    Track = track,
                    SpotifyTrack = mappedSpotifyTrack,
                };

                return bothTracks;

            }
            catch (Exception e)
            {
                _logger.LogInformation(e.Message);
            }

            return new SpotifyReturnModel();
        }

        public async Task<SpotifyReturnModel> searchSpotifyForTrackIds(List<TracksModel> tracks, SpotifyAccountsModel spotifyProfile)
        {
            var spotify = new SpotifyClient(spotifyProfile.access_token);

            var allSpotifyTracks = new List<SpotifyTracksModel>();
            var allTracks = new List<TracksModel>();

            foreach (var track in tracks)
            {
                string trackName = Regex.Replace(track.title, @"\([^()]*\)", string.Empty);

                string artistName = Regex.Replace(track.artist, @"(@|&|'|\(|\)|<|>|#)", string.Empty); //[ ](?=[ ])|[^-_,A-Za-z0-9 ]+

                string searchQuery = artistName.Trim() + ", " + trackName.Trim();

                _logger.LogInformation(searchQuery);

                SearchRequest request = new SearchRequest(SearchRequest.Types.Track, searchQuery)
                {
                    Limit = 15
                };

                var response = await spotify.Search.Item(request);

                List<FullTrack> responseItems;

                if (response.Tracks.Items.Count == 0) 
                {
                    string searchSecondQuery = trackName.Trim();

                    SearchRequest secondRequest = new SearchRequest(SearchRequest.Types.Track, searchSecondQuery)
                    {
                        Limit = 15
                    };

                    var secondResponse = await spotify.Search.Item(secondRequest);

                    responseItems = secondResponse.Tracks.Items;
                }
                else 
                {
                    responseItems = response.Tracks.Items;
                }

                foreach(var trackItem in responseItems)
                {
                    bool trackFound = false;

                    _logger.LogInformation("Title: " + track.title + " " + trackItem.Name);
                    _logger.LogInformation(Fuzz.PartialRatio(Regex.Replace(track.title.ToLower(), @"(@|&|'|\(|\)|<|>|#)", string.Empty), Regex.Replace(trackItem.Name.ToLower(), @"(@|&|'|\(|\)|<|>|#)", string.Empty)).ToString());
                   
                    if (Fuzz.PartialRatio(Regex.Replace(track.title.ToLower(), @"(@|&|'|\(|\)|<|>|#)", string.Empty), Regex.Replace(trackItem.Name.ToLower(), @"(@|&|'|\(|\)|<|>|#)", string.Empty)) > 70)
                    {
                        foreach(var artist in trackItem.Artists)
                        {
                            _logger.LogInformation("Artist: " + track.artist + " " + artist.Name);
                            _logger.LogInformation(Fuzz.PartialRatio(Regex.Replace(track.artist.ToLower(), @"\([^()]*\)+", string.Empty), Regex.Replace(artist.Name.ToLower(), @"\([^()]*\)", string.Empty)).ToString());

                            if (Fuzz.PartialRatio(Regex.Replace(track.artist.ToLower(), @"\([^()]*\)", string.Empty), Regex.Replace(artist.Name.ToLower(), @"\([^()]*\)", string.Empty)) > 75)
                            {
                                allTracks.Add(track);

                                var mappedSpotifyTracks = _mapper.Map<SpotifyTracksModel>(trackItem);

                                allSpotifyTracks.Add(mappedSpotifyTracks);

                                track.spotify_track_id = trackItem.Id;
                                track.spotify_uri = trackItem.Uri;

                                trackFound = true;                            

                                break;
                            }                           
                        }
                    }
                    
                    if (trackFound)
                    {
                        break;
                    }

                }

                if(responseItems.Count == 0) 
                {
                    continue;
                }              
            }

            var results = new SpotifyReturnModel
            {
                TracksUpdatedList = allTracks,
                SpotifyTracksList = allSpotifyTracks,
            };

            return results;
        }
        public async Task<FullPlaylist> createSpotifyPlaylist(string playlist_name, SpotifyAccountsModel spotifyProfile)
        {
            var spotify = new SpotifyClient(spotifyProfile.access_token);

            PlaylistCreateRequest request = new PlaylistCreateRequest(playlist_name)
            {
                Description = "Playlist built on MusicPlaylistBuilder.com"
            };

            var response = await spotify.Playlists.Create(spotifyProfile.spotify_account_id, request);

            return response;
        }

        public List<string> createUriTracksBody(List<string> trackUris) 
        {
            var uriObjectList = new List<string>();

            for (int i = 0; i < trackUris.Count; i += 100) 
            {
                List<string> currentTracks = trackUris.Skip(i).Take(100).ToList();

                var listOfUris = new SpotifyUriModel
                {
                    uris = new List<string>()
                };

                foreach (var trackUri in currentTracks.ToArray())
                {
                    if (trackUri == null)
                    {
                        continue;
                    }
                    else 
                    {
                        listOfUris.uris.Add(trackUri);
                    }                 
                }

                var jsonUriObject = JsonSerializer.Serialize(listOfUris);

                uriObjectList.Add(jsonUriObject);
            }

            return uriObjectList;
        }

        public async Task addTracksSpotifyPlaylist(string spotifyPlaylistId, string Uris ,SpotifyAccountsModel spotifyProfile)
        {
            var spotifyClient = _factory.CreateClient("Spotify");

            spotifyClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", spotifyProfile.access_token);

            var data = new StringContent(Uris, Encoding.UTF8, "application/json");

            await spotifyClient.PostAsync("playlists/" + spotifyPlaylistId + "/" + "tracks", data);
        }

        public async Task refreshSpotifyToken(IRepository<SpotifyAccountsModel> _repository, SpotifyAccountsModel spotifyProfile, IConfiguration _config)
        {
            var response = await new OAuthClient().RequestToken(
            new AuthorizationCodeRefreshRequest(_config["Spotify:clientId"], _config["Spotify:clientSecret"], spotifyProfile.refresh_token));

            spotifyProfile.access_token = response.AccessToken;
            spotifyProfile.expires_in = response.ExpiresIn;
            spotifyProfile.token_created = response.CreatedAt;

            await _repository.Update(spotifyProfile);
        }

        public bool expiredSpotifyTokenCheck(SpotifyAccountsModel spotifyAccount)
        {
            if (DateTime.Now.ToUniversalTime().AddSeconds(120) > spotifyAccount.token_created.AddSeconds(spotifyAccount.expires_in) || DateTime.Now.ToUniversalTime() > spotifyAccount.token_created.AddSeconds(spotifyAccount.expires_in))
            {
                return true;
            }

            return false;
        }

        //sleep on 429 response
    }
}

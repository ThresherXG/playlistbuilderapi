﻿using Microsoft.Extensions.Configuration;
using playlist_api.Data;
using playlist_api.Models;
using playlist_api.Models.Spotify;
using SpotifyAPI.Web;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace playlist_api.Services.Spotify
{
    public interface ISpotifyProcessor
    {
        Task<SpotifyReturnModel> scrapeSpotifyPlaylist(SpotifyPayloadModel spotifyPlaylist, SpotifyAccountsModel spotifyAccountModel);
        Task<SpotifyReturnModel> scrapeSpotifyTrack(SpotifyPayloadModel spotifyPlaylist, SpotifyAccountsModel spotifyAccountModel);
        Task<SpotifyReturnModel> scrapeSpotifyAlbum(SpotifyPayloadModel spotifyPlaylist, SpotifyAccountsModel spotifyAccountModel);
        Task<FullPlaylist> createSpotifyPlaylist(string playlist_name, SpotifyAccountsModel spotifyProfile);
        Task addTracksSpotifyPlaylist(string spotifyPlaylistId, string Uris, SpotifyAccountsModel spotifyProfile);
        Task<SpotifyReturnModel> searchSpotifyForTrackIds(List<TracksModel> tracks, SpotifyAccountsModel spotifyProfile);
        List<string> createUriTracksBody(List<string> tracks);
        Task refreshSpotifyToken(IRepository<SpotifyAccountsModel> _repository, SpotifyAccountsModel user, IConfiguration _config);
        bool expiredSpotifyTokenCheck(SpotifyAccountsModel spotifyAccount);
    }
}
